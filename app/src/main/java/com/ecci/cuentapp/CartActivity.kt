package com.ecci.cuentapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.cuentapp.adapters.CartAdapter
import com.ecci.cuentapp.adapters.CartProtocol
import com.ecci.cuentapp.data.Product
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity(), CartProtocol {

    lateinit var adapter: CartAdapter
    var total = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        val products = mutableListOf<Product>()
        products.add(Product("Televisor", 2_000_000, "Tecnología", R.drawable.ic_baseline_child_friendly_24, false))
        products.add(Product("Lavadora", 800_000, "Electrodoméstico Varios", R.drawable.logo_24, false))
        products.add(Product("Licuadora", 100_000, "Electrodoméstico Varios Con descuento", R.drawable.ic_baseline_card_giftcard_24, false))
        products.add(Product("Portatil", 2_500_000, "Tecnología", R.drawable.ic_baseline_clean_hands_24, false))


        adapter = CartAdapter(products, this)
        //Defino el adaptador
        productRecyclerView.adapter = adapter
        //Forma en que se va a mostrar esa lista
        productRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun didProductSelect(price: Int, isSelected: Boolean) {
        if(isSelected) total += price
        else total -= price
        totalTextView.text = total.toString()
        adapter.notifyDataSetChanged()
    }
}