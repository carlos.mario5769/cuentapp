package com.ecci.cuentapp

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.fragment_blue.*

class BlueFragment : Fragment(), MyProtocol {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_blue, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myButton.setOnClickListener {
            Toast.makeText(requireContext(), R.string.notification, Toast.LENGTH_LONG).show()
        }
        dialogButton.setOnClickListener {
            makeAlertDialog()
        }
        listButton.setOnClickListener {
            makeListDialog()
        }
        multiButton.setOnClickListener {
            makeMultiDialog()
        }
        dateButton.setOnClickListener {
            makeDateDialog()
        }
        timeButton.setOnClickListener {
            makeTimeDialog()
        }
        customButton.setOnClickListener {
            makeCustomDialog()
        }
        myCustomButton.setOnClickListener {
            makeMyCustomDialog()
        }
    }


    private fun makeAlertDialog() {
        //Mostrar Alert dialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Hola")
        builder.setMessage("¿Seguro que deseas salir de la aplicación")
        builder.setPositiveButton("Aceptar") { _, _ ->
            requireActivity().finish()
        }
        builder.setNegativeButton("Cancelar") { _,_ ->
            Toast.makeText(requireContext(), "gracias por no irte", Toast.LENGTH_LONG).show()
        }
        builder.setNeutralButton("Ver después", null)
        builder.setCancelable(false)
        builder.show()
    }

    private fun makeListDialog() {
        //Mostrar un diálogo en forma de lista
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Opciones")
        builder.setItems(
            arrayOf(
                "Rojo",
                "Verde",
                "Azul")
        ) { _, position ->
            when(position) {
                0 ->
                    Toast.makeText(requireContext(), "Seleccionó Rojo", Toast.LENGTH_SHORT).show()
                1 ->
                    Toast.makeText(requireContext(), "Seleccionó Verde", Toast.LENGTH_SHORT).show()
                else ->
                    Toast.makeText(requireContext(), "Seleccionó Azul", Toast.LENGTH_SHORT).show()
            }
        }
        builder.show()
    }

    private fun makeMultiDialog() {
        //Mostrar diálogo con selección múltiple
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Selecciona los acompañamientos")
        builder.setMultiChoiceItems(arrayOf(
            "Tomate",
            "Cebolla",
            "Salsas"), null) { dialog, position, isChecked ->
            if(isChecked)
                Toast.makeText(requireContext(), "Seleccionó la opción ".plus(position), Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(requireContext(), "Se deseleccionó la opción ".plus(position), Toast.LENGTH_SHORT).show()
        }
        builder.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun makeDateDialog() {
        //Mostrar DatePicker
        val dialog = DatePickerDialog(requireContext(), { dialog, year, month, day ->
            Toast.makeText(requireContext(), "Seleccionó el año ".plus(year).plus(" con el mes ").plus(month).plus(" y con el día ").plus(day), Toast.LENGTH_SHORT).show()
        }, 2021, 2, 18)
        dialog.show()
    }

    private fun makeTimeDialog() {
        //Mostrar Time dialog
        val dialog = TimePickerDialog(requireContext(), { dialog,hour,minute ->
            Toast.makeText(requireContext(), "Seleccionó la hora ".plus(hour).plus(":").plus(minute), Toast.LENGTH_SHORT).show()
        }, 9, 28, false)
        dialog.setCancelable(false)
        dialog.setTitle("Seleccione la hora")
        dialog.setMessage("Selecciona la hora usando este diálogo")
        dialog.show()
    }

    private fun makeCustomDialog() {
        //Mostrar Custom dialog
        val dialogView = layoutInflater.inflate(R.layout.custom_toast, null)
        val builder = AlertDialog.Builder(requireContext())

        builder.setTitle("Hola")
        builder.setMessage("¿Seguro que deseas salir de la aplicación")
        builder.setView(dialogView)
        val dialog= builder.show()

        val button = dialogView.findViewById<Button>(R.id.myButtonDialog)
        button.setOnClickListener {
            val intent = Intent(requireActivity(), HomeActivity::class.java)
            requireActivity().startActivity(intent)
        }
    }

    private fun makeMyCustomDialog() {
        val dialog = MyCustomDialog(requireContext(), "Error desconocido", "Por favor, vuelve a intentarlo \n Si el error persiste, comunciate con nosotros", this)
        dialog.show()
    }

    override fun changeText() {
        myButton.isVisible = false
        listButton.isVisible = false
    }
}