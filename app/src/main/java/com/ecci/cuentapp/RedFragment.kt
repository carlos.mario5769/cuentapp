package com.ecci.cuentapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_red.*


class RedFragment(private val myText: String, val delegate: MyProtocol, val listener: (String) -> Unit) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_red, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textView.text = myText

        callbackButton.setOnClickListener {
            listener("VERDE")
        }
        delegateButton.setOnClickListener {
            delegate.changeText()
        }
    }
}