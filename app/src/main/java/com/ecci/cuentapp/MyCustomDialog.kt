package com.ecci.cuentapp

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.dialog_my_custom.*

class MyCustomDialog(context: Context, var myTitle: String, var myMessage: String, val myProtocol: MyProtocol): Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_my_custom)
        setCancelable(false)
        setTitle("Soy el título")

        setupButton()
        titleTextView.text = myTitle
        messageTextView.text = myMessage
    }

    fun setupButton() {

        myButton.setOnClickListener {
            myProtocol.changeText()
        }

        closeButton.setOnClickListener {
            dismiss()
        }
    }

}