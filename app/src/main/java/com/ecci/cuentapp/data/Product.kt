package com.ecci.cuentapp.data

data class Product(val name: String, val price: Int, val category: String, val imageDrawable: Int, var isSelected: Boolean)