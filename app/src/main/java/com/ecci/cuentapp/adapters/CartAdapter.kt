package com.ecci.cuentapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.ecci.cuentapp.R
import com.ecci.cuentapp.data.Product
import kotlinx.android.synthetic.main.item_product.view.*

//Este adapter es el 'coordinador' de mi lista
class CartAdapter(val list: List<Product>, var delegate: CartProtocol): RecyclerView.Adapter<CartAdapter.CardViewHolder>() {

    //Esta clase es la representación de cada celda
    class CardViewHolder(itemView: View, var delegate: CartProtocol) : RecyclerView.ViewHolder(itemView) {
        fun bind(product: Product) {
            itemView.nameTextView.text = product.name
            itemView.categoryTextView.text = product.category
            itemView.priceTextView.text = "$".plus(product.price)
            itemView.iconImageView.setImageDrawable(itemView.resources.getDrawable(product.imageDrawable))
            itemView.setOnClickListener {
                product.isSelected = !product.isSelected
                //No se ejecuta aquí, si no en el delegado, o sea, la actividad
                delegate.didProductSelect(product.price, product.isSelected)
            }
            if(product.isSelected) {
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.light_green))
            } else {
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.white))
            }
        }
    }

    //Se encarga de definir el layout de la celda
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return CardViewHolder(view, delegate)
    }

    //Define el número de celda
    override fun getItemCount(): Int {
        return list.size
    }

    //Envía la información para cada celda
    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val product = list[position]
        holder.bind(product)
    }
}

interface CartProtocol {
    fun didProductSelect(price: Int, isSelected: Boolean)
}