package com.ecci.cuentapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MyProtocol {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstButton.setOnClickListener {
            showRedFragment()
        }
        secondButton.setOnClickListener {
            showBlueFragment()
        }
        thirdButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            //Añadirle extras
            intent.putExtra("maintitle", "Texto enviado desde un extra en el intent")
            intent.putExtra("number", 45)
            intent.putExtra("booleanos", true)
            intent.putExtra("una_lista", arrayOf("Hola", "chao"))
            startActivity(intent)
        }
    }

    private fun showRedFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val redFragment = RedFragment("Texto enviado desde la actividad", this) { par1 ->
            firstButton.text = par1
        }
        transaction.replace(R.id.container, redFragment, null)
        transaction.commit()
    }

    private fun showBlueFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, BlueFragment(), null)
        transaction.commit()
    }

    fun showGreenFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, GreenFragment(), null)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun changeText() {
        secondButton.text = "ROSA"
    }

}